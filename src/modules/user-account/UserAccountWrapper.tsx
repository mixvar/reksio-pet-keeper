import { Row } from 'app/components/Grid';
import { FutureResult } from 'app/utils';
import { Text } from 'grommet';
import { useUser } from 'modules/user';
import { UserData } from 'modules/user/types';
import React from 'react';
import useReactRouter from 'use-react-router';
import { userImagesRepo } from 'app/repositories';
import { UserAccount } from '.';
import Alert from 'app/alerts';

type Props = {};

export const UserAccountWrapper: React.FC<Props> = () => {
    const router = useReactRouter();
    const user = useUser();
    const [profilePicUrl, setProfilePicUrl] = React.useState<string>();

    React.useEffect(() => {
        if (user) {
            userImagesRepo.getProfilePictureUrl(user.id).then(setProfilePicUrl);
        } else {
            router.history.replace('/');
        }
    }, [user]);

    if (!user) {
        return null;
    }

    const onProfilePicChange = (image: File) =>
        userImagesRepo
            .uploadProfilePicture(user.id, image)
            .then(setProfilePicUrl)
            .catch(() =>
                Alert.error('Wystąpił błąd podczas przesyłania zdjęcia!')
            );

    const render = (userData: UserData) => (
        <UserAccount
            userData={userData}
            profilePicUrl={profilePicUrl}
            onProfilePicChange={onProfilePicChange}
            updateUserData={user.updateData}
            logout={user.logout}
        />
    );

    return FutureResult.reduce(user.data, {
        initial: () => null,

        loading: () => null, // TODO pretty spinner

        success: render,

        updating: render,

        failure: err => (
            <Row pad="XL" center="xs">
                <Text color="status-critical">{err.message}</Text>
            </Row>
        ),
    });
};
