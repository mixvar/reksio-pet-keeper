import React from 'react';

//todo replace icon with reksio
export const ToastAlert = ({ message }: { message: String }) => (
    <div>
        <span>🐶</span>
        {message}
    </div>
);
