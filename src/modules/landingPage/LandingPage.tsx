import React from 'react';
import styled from 'styled-components/macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Col, Row } from 'app/components/Grid';
import { Colors } from 'app/styles/colors';
import logo from '../../app/assets/logo_kp.png';

const StyledMessage = styled.h1`
    font-size: 60px;
    font-weight: 500;
    font-weight: bold;
`;
const StyledMarkerIcon = styled(FontAwesomeIcon)`
    font-size: 200px;
    font-weight: 500;
    color: ${Colors.primary};
    margin-right: 20px;
`;
const StyledSmallMarkerIcon = styled(FontAwesomeIcon)`
    display: inline-block;
    font-size: 60px;
    color: ${Colors.primary};
`;

const StyledText = styled.p`
    font-size: 20px;
    text-align: center;
`;

const StyledLogo = styled.img`
    width: 100%;
`;

const StyledA = styled.a`
    display: inline-block;
    height: 100px;
    width: 200px;
`;

type Props = {};

export const LandingPage: React.FC<Props> = () => {
    return (
        <div>
            <Row pad="L">
                <Col xs={7} lg={12}>
                    <Row padY="XL" middle="xs" nowrap>
                        <StyledMarkerIcon icon="map-marker-alt" />
                        <StyledMessage>
                            Zostań opiekunem. <br /> Zdobądź więcej klientów
                        </StyledMessage>
                    </Row>
                </Col>

                <Col xs={12}>
                    <Row pad="L" around="xs">
                        <Col>
                            <Row padY="L" middle="xs">
                                <StyledSmallMarkerIcon icon="map-marker-alt" />
                                <StyledText>
                                    Zapewniamy <br /> opiekę transakcji
                                </StyledText>
                            </Row>
                        </Col>
                        <Col>
                            <Row padY="L" middle="xs">
                                <StyledText>Współpracujemy z</StyledText>
                                <StyledA
                                    target="_blank"
                                    href="https://www.karmimypsiaki.pl/"
                                >
                                    <StyledLogo
                                        src={logo}
                                        alt="Karmimy Psiaki Logo"
                                    />
                                </StyledA>
                            </Row>
                        </Col>
                        <Col>
                            <Row padY="L" middle="xs">
                                <StyledSmallMarkerIcon icon="map-marker-alt" />
                                <StyledText>
                                    Budujemy Twoją <br /> markę przez opinię{' '}
                                    <br /> zadowolonych właścicieli
                                </StyledText>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    );
};
