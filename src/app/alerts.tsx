import React from 'react';
import { toast } from 'react-toastify';

type AlertProps = {
    message: string;
};

const ToastAlert: React.FC<AlertProps> = ({ message }) => (
    <div>
        <span>🐶</span>
        {message}
    </div>
);

export default {
    info: (message: string) => toast(<ToastAlert message={message} />),

    error: (message: string) => toast.error(<ToastAlert message={message} />),

    success: (message: string) =>
        toast.success(<ToastAlert message={message} />),
};
