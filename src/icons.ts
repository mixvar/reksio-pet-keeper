import { library } from '@fortawesome/fontawesome-svg-core';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import {
    faFacebookSquare,
    faInstagram,
} from '@fortawesome/free-brands-svg-icons';

library.add(faMapMarkerAlt);
library.add(faFacebookSquare);
library.add(faInstagram);
