import React from 'react';
import styled from 'styled-components';
import useReactRouter from 'use-react-router';

import { MapOverlay } from 'modules/map/MapOverlay';
import { useUser } from 'modules/user';
import { Login } from './Login';

const StyledMapOverlay = styled(MapOverlay)`
    background: rgba(255, 255, 255, 0.5);
`;

export const LoginMapOverlay: React.FC = () => {
    const { location } = useReactRouter();
    const user = useUser();

    const visible = location.pathname === '/zostan-opiekunem' && user == null;

    if (!visible) return null;

    return (
        <StyledMapOverlay fill blocking>
            <Login />
        </StyledMapOverlay>
    );
};
