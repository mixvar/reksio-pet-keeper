import React from 'react';
import styled from 'styled-components';
import { Col, Row } from 'app/components/Grid';
import { Map } from './Map';
import { MapVisibility, useMap } from './MapContext';
import { LoginMapOverlay } from 'modules/user';
import { AboutMapOverlay } from '../about';

const ScrollableCol = styled(Col)`
    height: 100vh;
    overflow-y: auto;
`;

type Props = {};

// TODO animations
// TODO mobile
export const MapLayout: React.FC<Props> = ({ children }) => {
    const { mapVisibility } = useMap();

    const map = (
        <Map>
            <LoginMapOverlay />
            <AboutMapOverlay />
        </Map>
    );

    switch (mapVisibility) {
        case MapVisibility.Hidden:
            return <>{children}</>;

        case MapVisibility.Small:
            return (
                <Row>
                    <ScrollableCol xs={8}>{children}</ScrollableCol>
                    <Col xs={4}>{map}</Col>
                </Row>
            );

        case MapVisibility.Large:
            return (
                <Row>
                    <ScrollableCol xs={5}>{children}</ScrollableCol>
                    <Col xs={7}>{map}</Col>
                </Row>
            );
    }
};
