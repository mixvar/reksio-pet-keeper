import firebase from 'firebase';

export type LoginResult =
    | { type: 'success'; credentials: firebase.auth.UserCredential }
    | { type: 'accountAlreadyExistsError'; currentProvider: string }
    | { type: 'unknownError'; message: string };

export const authService = {
    async googleLogin(): Promise<LoginResult> {
        const provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().languageCode = 'pl';

        return firebase
            .auth()
            .setPersistence(firebase.auth.Auth.Persistence.LOCAL)
            .then(() => signIn(provider));
    },

    async facebookLogin(): Promise<LoginResult> {
        const provider = new firebase.auth.FacebookAuthProvider();
        provider.addScope('public_profile');
        provider.addScope('email');
        firebase.auth().languageCode = 'pl';

        return firebase
            .auth()
            .setPersistence(firebase.auth.Auth.Persistence.LOCAL)
            .then(() => signIn(provider));
    },

    async logout(): Promise<void> {
        return firebase.auth().signOut();
    },
};

async function signIn(
    provider: firebase.auth.AuthProvider
): Promise<LoginResult> {
    return firebase
        .auth()
        .signInWithPopup(provider)
        .then((credentials): LoginResult => ({ type: 'success', credentials }))
        .catch(handleAuthError);
}

async function handleAuthError(error: any): Promise<LoginResult> {
    if (error.code === 'auth/account-exists-with-different-credential') {
        return firebase
            .auth()
            .fetchSignInMethodsForEmail(error.email)
            .then(methods => ({
                type: 'accountAlreadyExistsError',
                currentProvider: methods[0],
            }));
    }
    return Promise.resolve({ type: 'unknownError', message: error.message });
}
