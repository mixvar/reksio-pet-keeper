import { KeeperServices } from './keeper-service';

export interface UserData {
    readonly firstName: string;
    readonly lastName: string;
    readonly email: string | null;
    readonly address: string | null;
    readonly phone: string | null;
    readonly about: string | null;
    readonly services: KeeperServices;
}
