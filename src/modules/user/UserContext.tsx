import React from 'react';
import firebase from 'firebase';

import { FutureResult } from 'app/utils';
import { userDataRepo } from 'app/repositories';
import { authService } from './auth-service';
import { UserData, KeeperServiceKind } from './types';

export type UserContextType = {
    id: string;
    data: FutureResult.Type<UserData>;
    updateData(data: Partial<UserData>): Promise<void>;
    logout(): void;
} | null;

const ReactContext = React.createContext<UserContextType | undefined>(
    undefined
);

const USER_ID_KEY = 'userId';

export const UserContextProvider: React.FC = ({ children }) => {
    const [id, setId] = React.useState<string | null>(
        localStorage.getItem(USER_ID_KEY)
    );

    const [data, setData] = React.useState<FutureResult.Type<UserData>>(
        FutureResult.initial()
    );

    function handleLogOut() {
        setId(null);
        setData(FutureResult.initial());
        localStorage.removeItem(USER_ID_KEY);
    }
    async function handleLogIn(firebaseUser: firebase.User) {
        setId(firebaseUser.uid);
        setData(FutureResult.loading());
        localStorage.setItem(USER_ID_KEY, firebaseUser.uid);

        getUserData(firebaseUser)
            .then(resp => setData(FutureResult.success(resp)))
            .catch(err => setData(FutureResult.failure(err as Error)));
    }

    React.useEffect(() => {
        return firebase.auth().onAuthStateChanged(user => {
            if (user) {
                handleLogIn(user);
            } else {
                handleLogOut();
            }
        });
    }, []);

    const ctxValue = React.useMemo<UserContextType>(() => {
        if (id == null) {
            return null;
        }

        return {
            id,
            data,
            updateData: async value => {
                if (FutureResult.isSuccess(data)) {
                    setData(FutureResult.updating(data.payload));
                }

                return userDataRepo
                    .update(id, value)
                    .then(() => {
                        if (FutureResult.isUpdating(data)) {
                            setData(
                                FutureResult.success({
                                    ...data.payload,
                                    ...value,
                                })
                            );
                        }
                    })
                    .catch(err => setData(FutureResult.failure(err as Error)));
            },
            logout: authService.logout,
        };
    }, [id, data]);

    return <ReactContext.Provider value={ctxValue} children={children} />;
};

async function getUserData(user: firebase.User): Promise<UserData> {
    const data = await userDataRepo.get(user.uid);
    if (data) {
        return data;
    }
    const displayName = user.displayName!.split(' ');
    const initialData: UserData = {
        firstName: displayName[0],
        lastName: displayName[1],
        email: user.email || null,
        phone: user.phoneNumber || null,
        about: null,
        address: null,
        services: {
            [KeeperServiceKind.PETSITTING_KEEPER_HOME]: null,
            [KeeperServiceKind.PETSITTING_PET_HOME]: null,
            [KeeperServiceKind.WALKING]: null,
        },
    };
    await userDataRepo.set(user.uid, initialData);
    return initialData;
}

export function useUser(): UserContextType {
    const context = React.useContext(ReactContext);
    if (context === undefined) {
        throw new Error(`useUserCtx must be used within a UserContextProvider`);
    }
    return context;
}
