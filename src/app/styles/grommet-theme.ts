import { ThemeValue } from 'grommet';
import { AddCircle, SubtractCircle } from 'grommet-icons';

import { Colors } from './colors';

export const theme: ThemeValue = {
    global: {
        colors: {
            active: Colors.primary,
            brand: Colors.primary,
            focus: Colors.primary,
            primary: Colors.primary,
        },
    },
    accordion: {
        icons: {
            collapse: SubtractCircle,
            expand: AddCircle,
            color: Colors.primary,
        },
        border: undefined,
    },
    checkBox: {
        color: Colors.selected,
    },
    layer: {
        zIndex: '100000',
        container: {
            zIndex: '100000',
        },
    },
};
