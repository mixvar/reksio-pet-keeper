import React from 'react';
import { Grommet, ThemeContext } from 'grommet';
import { BrowserRouter as Router } from 'react-router-dom';

import { GlobalStyles, theme } from 'app/styles';
import { UserContextProvider } from 'modules/user';
import { MapContextProvider } from 'modules/map';
import { Layout } from 'app/components';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

export const App: React.FC = () => {
    return (
        <Grommet plain>
            <ThemeContext.Extend value={theme}>
                <Router>
                    <UserContextProvider>
                        <MapContextProvider>
                            <GlobalStyles />
                            <Layout />
                            <ToastContainer
                                hideProgressBar={true}
                                autoClose={5000}
                            />
                        </MapContextProvider>
                    </UserContextProvider>
                </Router>
            </ThemeContext.Extend>
        </Grommet>
    );
};
