import React from 'react';
import { Route } from 'react-router-dom';
import styled from 'styled-components/macro';

import { HomePage } from 'modules/homepage';
import { About } from 'modules/about';
import { UserAccountWrapper } from 'modules/user-account';
import { Header } from 'modules/header';
import { MapLayout } from 'modules/map';
import { Login } from 'modules/user';
import { Footer } from 'modules/footer';
import { LandingPage } from '../../modules/landingPage';

type Props = {};

const HEADER_HEIGHT = 130;
const FOOTER_HEIGHT = 40;

const StyledContent = styled.main`
    min-height: calc(100vh - ${HEADER_HEIGHT}px - ${FOOTER_HEIGHT}px);
`;

export const Layout: React.FC<Props> = () => (
    <MapLayout>
        <Header height={HEADER_HEIGHT} />
        <StyledContent>
            <Route path="/" exact component={HomePage} />
            <Route path="/about" component={About} />
            <Route path="/account" component={UserAccountWrapper} />
            <Route path="/login" component={Login} />
            <Route path="/zostan-opiekunem" component={LandingPage} />
        </StyledContent>
        <Footer height={FOOTER_HEIGHT} />
    </MapLayout>
);
