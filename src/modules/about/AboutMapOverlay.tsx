import React from 'react';
import styled from 'styled-components';
import useReactRouter from 'use-react-router';
import { MapOverlay } from 'modules/map/MapOverlay';
import cat from 'app/assets/cat1.jpeg';

const Cat = styled.div`
    width: 100%;
    height: 100%;
    background-image: url(${cat});
    background-size: cover;
    background-position: center;
`;

export const AboutMapOverlay: React.FC = () => {
    const { location } = useReactRouter();

    const visible = location.pathname === '/about';

    if (!visible) return null;

    return (
        <MapOverlay fill blocking>
            <Cat />
        </MapOverlay>
    );
};
