import { Colors } from 'app/styles/colors';
import { Button, TextInput } from 'grommet';
import { Close } from 'grommet-icons';
import React from 'react';
import styled from 'styled-components';
import { SimpleModal } from 'app/components';

const StyledTextInput = styled(TextInput)`
    color: #fff;
    padding: 7px;
    display: inline-block;
    border: #fff;
`;

type Props = {
    open: boolean;

    onClose(): void;
    onImageSelected(image: File): void;
};

export const ImagePicker: React.FC<Props> = props => {
    const [file, setFile] = React.useState<File | null>(null);

    return (
        <SimpleModal open={props.open} onClose={props.onClose}>
            <StyledTextInput
                onChange={event => setFile(event.target!.files![0])}
                type="file"
            />
            <Button
                onClick={() => props.onImageSelected(file!)}
                label={'przeslij obraz'}
                color={Colors.primary}
                disabled={file == null}
            />
        </SimpleModal>
    );
};
