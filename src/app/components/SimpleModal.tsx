import { Box, Button, Layer } from 'grommet';
import { Close } from 'grommet-icons';
import React from 'react';
import styled from 'styled-components/macro';
import { Row } from 'app/components/Grid';

export const StyledLayer = styled(Layer)`
    min-width: 350px;
`;

export const StyledButtonClose = styled(Button)`
    position: absolute;
    right: 0;
    top: 0;
    svg {
        transition: all 200ms ease;
        transform: scale(0.8);
    }
    :hover {
        svg {
            stroke: red;
            transform: scale(1);
        }
    }
`;

type Props = {
    open: boolean;
    onClose(): void;
};

export const SimpleModal: React.FC<Props> = props => {
    if (!props.open) {
        return null;
    }

    return (
        <StyledLayer onEsc={props.onClose} onClickOutside={props.onClose}>
            <Box elevation="xlarge">
                <StyledButtonClose icon={<Close />} onClick={props.onClose} />
                <Box pad="medium">{props.children}</Box>
            </Box>
        </StyledLayer>
    );
};
