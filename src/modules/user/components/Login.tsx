import React from 'react';
import { Button } from 'grommet';

import { authService, LoginResult } from '../auth-service';
import { Row, Col } from 'app/components/Grid';
import { Colors } from 'app/styles';
import { FancyHeading } from 'app/components';
import Alert from 'app/alerts';

type Props = {};

export const Login: React.FC<Props> = props => {
    const handleLoginResult = (result: LoginResult) => {
        if (result.type === 'success') {
            Alert.info('Zalogowano!');
        }
        if (result.type === 'accountAlreadyExistsError') {
            Alert.error(
                'Posiadasz już konto! zaloguj sie używając ' +
                    result.currentProvider
            );
        }
        if (result.type === 'unknownError') {
            Alert.error('Logowanie nie powiodło się!');
            console.warn(result.message);
        }
    };

    const onGoogleLogin = () =>
        authService.googleLogin().then(handleLoginResult);

    const onFacebookLogin = () =>
        authService.facebookLogin().then(handleLoginResult);

    return (
        <Row fill center="xs" middle="xs">
            <Col>
                <Row center="xs">
                    <FancyHeading>Dołącz do nas!</FancyHeading>
                </Row>
                <Row padY="S">
                    <Button
                        onClick={onGoogleLogin}
                        color={Colors.google}
                        label="Zaloguj z Google"
                        fill="horizontal"
                        primary
                    />
                </Row>
                <Row padY="S">
                    <Button
                        onClick={onFacebookLogin}
                        color={Colors.facebook}
                        label="Zaloguj z Facebook"
                        fill="horizontal"
                        primary
                    />
                </Row>
            </Col>
        </Row>
    );
};
