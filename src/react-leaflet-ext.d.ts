import { LeafletContext, Map } from 'react-leaflet';

declare module 'react-leaflet' {
    export function useLeaflet(): LeafletContext;
}
