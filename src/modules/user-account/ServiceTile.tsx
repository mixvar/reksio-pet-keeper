import React from 'react';
import styled from 'styled-components';
import {
    Box,
    CheckBox,
    TextInput,
    TextArea,
    Heading,
    Collapsible,
} from 'grommet';

import { FancyHeading } from 'app/components';
import { Row, Col } from 'app/components/Grid';
import {
    KeeperServiceKind,
    KeeperServiceOptions,
    PetKind,
} from 'modules/user/types';
import { Spacing } from 'app/styles';

const StyledServiceName = styled(Heading)`
    margin: 0;
`;

const StyledFieldset = styled.fieldset`
    width: 100%;
    border: 1px solid rgba(0, 0, 0, 0.33);
    label {
        margin-bottom: ${Spacing.S};
    }
`;

type Props = {
    kind: KeeperServiceKind;
    data: KeeperServiceOptions;
    onChange(data: KeeperServiceOptions): void;
};

const propsEqual = (p1: Props, p2: Props): boolean =>
    p1.kind === p2.kind && p1.data === p2.data;

export const ServiceTile = React.memo<Props>(({ kind, data, onChange }) => {
    function toggleActive() {
        onChange({ ...data, active: !data.active });
    }
    function setNotes(e: React.ChangeEvent<HTMLTextAreaElement>) {
        onChange({ ...data, notes: e.target.value });
    }

    function renderPetKindCheckboxes(): React.ReactNode {
        const toggleApplicablePet = (kind: PetKind) => {
            const applicablePets = data.applicablePets.includes(kind)
                ? data.applicablePets.filter(k => k !== kind)
                : data.applicablePets.concat(kind);
            onChange({ ...data, applicablePets });
        };

        const checkboxes: Record<PetKind, string> = {
            [PetKind.CAT]: 'koty',
            [PetKind.SMALL_DOG]: 'małe psy',
            [PetKind.LARGE_DOG]: 'duże psy',
        };

        return (
            <StyledFieldset>
                <legend>zwierzaki</legend>
                {Object.entries(checkboxes).map(([kind, label]) => (
                    <CheckBox
                        key={kind}
                        checked={data.applicablePets.includes(kind as PetKind)}
                        label={label}
                        onChange={() => toggleApplicablePet(kind as PetKind)}
                    />
                ))}
            </StyledFieldset>
        );
    }

    function renderPriceInput(): React.ReactNode {
        const onPriceChange = (e: any) => {
            const price = Number(e.target.value);
            const pricePerDay =
                kind === KeeperServiceKind.WALKING ? price * 24 : price;
            onChange({ ...data, pricePerDay });
        };
        const displayPrice =
            kind === KeeperServiceKind.WALKING
                ? data.pricePerDay / 24
                : data.pricePerDay;
        const label =
            kind === KeeperServiceKind.WALKING
                ? 'Cena za godzinę'
                : 'Cena za dzień';
        const inputId = `${kind}-price`;

        return (
            <>
                <label htmlFor={inputId}>{label}</label>
                <TextInput
                    id={inputId}
                    min={1}
                    type="number"
                    value={displayPrice || ''}
                    onChange={onPriceChange}
                />
            </>
        );
    }

    return (
        <Col pad="M" md={12} lg={4}>
            <Box elevation="small" pad={Spacing.L} fill>
                <Row padBot="L" center="xs">
                    <StyledServiceName level={4}>
                        {serviceName(kind)}
                    </StyledServiceName>
                </Row>
                <Row padY="S">
                    <CheckBox
                        checked={data.active}
                        label={data.active ? 'aktywne' : 'nieaktywne'}
                        onChange={toggleActive}
                        toggle
                    />
                </Row>

                <Collapsible open={data.active}>
                    <Row padY="S">{renderPetKindCheckboxes()}</Row>
                    <Row padY="S">{renderPriceInput()}</Row>
                    <Row padY="S">
                        <TextArea
                            rows={3}
                            placeholder="Dodatkowe informacje"
                            value={data.notes}
                            onChange={setNotes}
                        />
                    </Row>
                </Collapsible>
            </Box>
        </Col>
    );
}, propsEqual);

function serviceName(kind: KeeperServiceKind): string {
    switch (kind) {
        case KeeperServiceKind.PETSITTING_KEEPER_HOME:
            return 'Opieka w moim domu';
        case KeeperServiceKind.PETSITTING_PET_HOME:
            return 'Opieka w domu zwierzaka';
        case KeeperServiceKind.WALKING:
            return 'Wyprowadzanie na spacer';
    }
}
