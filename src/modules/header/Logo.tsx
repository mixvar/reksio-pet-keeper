import React from 'react';
import styled from 'styled-components/macro';
import { Row, Col } from 'app/components/Grid';

import logo from 'app/assets/reksio-logo.png';

const StyledLogo = styled.img`
    width: 100px;
    padding: 0 10px;
    margin-top: 10px;
`;
const StyledTitle = styled.div`
    font-size: 50px;
    font-family: 'Oswald', sans-serif;
`;
const StyledSubTitle = styled.div`
    font-size: 30px;
    line-height: 0.8;
    font-family: 'Oswald', sans-serif;
`;

type Props = {};

const Logo: React.FC<Props> = () => (
    <Row top="xs">
        <Col>
            <StyledLogo src={logo} alt="Reksio Logo" />
        </Col>
        <Col>
            <StyledTitle>REKSIO</StyledTitle>
            <StyledSubTitle>Pet Keeper</StyledSubTitle>
        </Col>
    </Row>
);

export default Logo;
