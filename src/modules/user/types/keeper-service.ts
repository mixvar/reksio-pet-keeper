export type KeeperServices = Record<
    KeeperServiceKind,
    KeeperServiceOptions | null
>;

export interface KeeperServiceOptions {
    readonly active: boolean;
    readonly applicablePets: PetKind[];
    readonly pricePerDay: number;
    readonly notes: string;
}

export enum KeeperServiceKind {
    PETSITTING_KEEPER_HOME = 'PETSITTING_KEEPER_HOME',
    PETSITTING_PET_HOME = 'PETSITTING_PET_HOME',
    WALKING = 'WALKING',
}

export enum PetKind {
    CAT = 'CAT',
    SMALL_DOG = 'SMALL_DOG',
    LARGE_DOG = 'LARGE_DOG',
}
