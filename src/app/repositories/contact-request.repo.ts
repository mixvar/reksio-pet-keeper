import firebase, { firestore } from 'firebase';

const db = firebase.firestore();

export interface ContactRequest {
    mail: string;
    message: string;
}

class ContactRequestRepo {
    constructor(private db: firestore.Firestore) {}

    async push(request: ContactRequest): Promise<void> {
        const key = request.mail + '##' + new Date().getTime();

        return this.collection.doc(key).set(request);
    }

    private get collection() {
        return this.db.collection('contactRequest');
    }
}

export const contactRequestRepo = new ContactRequestRepo(db);
