import { SimpleModal } from 'app/components';
import { Col, Row } from 'app/components/Grid';
import { Colors } from 'app/styles/colors';
import { Button } from 'grommet';
import Logo from 'modules/header/Logo';
import { useUser } from 'modules/user';
import { Login } from 'modules/user/components/Login';
import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components/macro';
import useReactRouter from 'use-react-router';

const StyledHeader = styled(Row)<{ height: number }>`
    min-height: ${({ height }) => `${height}px`};
    padding: 8px;
`;
const PlainLink = styled(Link)`
    text-decoration: none;
    color: inherit;
`;

const StyledButton = styled(Button)`
    font-size: 25px;
`;

type Props = {
    height: number;
};

export const Header: React.FC<Props> = ({ height }) => {
    const user = useUser();
    const router = useReactRouter();
    const path = router.location.pathname;
    const [loginModalOpen, setLoginModalOpen] = React.useState();

    React.useEffect(() => {
        if (user) {
            setLoginModalOpen(false);
        }
    }, [user]);

    function getColor(activePath: string): string {
        if (path === activePath) {
            return Colors.primary;
        } else {
            return Colors.foreground;
        }
    }

    return (
        <>
            <StyledHeader height={height} around="xs">
                <Col>
                    <PlainLink to="/">
                        <Logo />
                    </PlainLink>
                </Col>
                <Row middle="xs">
                    <Col pad="M">
                        {!user && (
                            <Link to="zostan-opiekunem">
                                <StyledButton
                                    label="Zostań opiekunem"
                                    plain
                                    color={getColor('/zostan-opiekunem')}
                                />
                            </Link>
                        )}
                    </Col>

                    <Col pad="M">
                        {user && (
                            <Link to="/account">
                                <StyledButton
                                    label="Moje konto"
                                    plain
                                    color={getColor('/account')}
                                />
                            </Link>
                        )}
                    </Col>
                    <Col>
                        {!user && (
                            <>
                                <StyledButton
                                    plain
                                    label="Zaloguj się"
                                    color={Colors.foreground}
                                    onClick={() => setLoginModalOpen(true)}
                                />
                            </>
                        )}
                    </Col>

                    <Col pad="M">
                        <Link to="/about">
                            <StyledButton
                                label="O nas"
                                plain
                                color={getColor('/about')}
                            />
                        </Link>
                    </Col>
                </Row>
            </StyledHeader>

            <SimpleModal
                open={loginModalOpen}
                onClose={() => setLoginModalOpen(false)}
            >
                <Login />
            </SimpleModal>
        </>
    );
};
