import React from 'react';
import { Heading } from 'grommet';
import styled from 'styled-components/macro';

import { Colors, Spacing } from 'app/styles';

const StyledHeading = styled(Heading)<{ small?: boolean }>`
    font-size: ${props => (props.small ? '20px' : '28px')};
    position: relative;
    margin: 0;
    line-height: normal;
    margin: 4px;
`;
const HeadingShape = styled.div<{ small?: boolean }>`
    position: absolute;
    width: 110%;
    height: 50%;
    background: ${Colors.primary};
    top: ${props => (props.small ? '15px' : '18px')};
    left: -20px;
    border-radius: 5px;
    ${props => props.small && 'opacity: 0.75'};
`;

type Props = {
    classname?: string;
    small?: boolean;
};

export const FancyHeading: React.FC<Props> = ({
    children,
    classname,
    small,
}) => (
    <StyledHeading className={classname} small={small}>
        <HeadingShape small={small} />
        <span style={{ position: 'relative' }}>{children}</span>
    </StyledHeading>
);
