import React from 'react';
import { Map as LeafletMap, Marker, TileLayer } from 'react-leaflet';
import L from 'leaflet';
import { useMap } from './MapContext';

type Coords = {
    lat: number;
    lng: number;
};

const cracowCoords: Coords = {
    lat: 50.06,
    lng: 19.94,
};

const ico = new L.Icon({
    iconUrl: require('app/assets/reksio_marker.png'),
    iconSize: new L.Point(60, 70),
});

type Props = {};

function extract(
    location: Position,
    latOffset: number = 0,
    lonOffset: number = 0
): [number, number] {
    if (location == null) return [cracowCoords.lat, cracowCoords.lng];
    return [
        location.coords.latitude + latOffset,
        location.coords.longitude + lonOffset,
    ];
}

export const Map: React.FC<Props> = ({ children }) => {
    const { location } = useMap();
    const [zoom, setZoom] = React.useState(15);

    return (
        <LeafletMap
            center={extract(location)}
            zoom={zoom}
            style={{ height: '100vh' }}
        >
            <TileLayer
                provider="CartoDB.Voyager"
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'
                url="https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png"
            />
            {location && (
                <div>
                    <Marker position={extract(location)} icon={ico} />
                    <Marker
                        position={extract(location, 0.005, 0.005)}
                        icon={ico}
                    />
                    <Marker
                        position={extract(location, -0.004, 0.006)}
                        icon={ico}
                    />
                    <Marker
                        position={extract(location, 0, -0.008)}
                        icon={ico}
                    />
                </div>
            )}
            {children}
        </LeafletMap>
    );
};
