import firebase, { firestore } from 'firebase';
import { UserData } from 'modules/user/types';

const db = firebase.firestore();

class UserDataRepo {
    constructor(private db: firestore.Firestore) {}

    async get(userId: string): Promise<UserData | null> {
        return this.collection
            .doc(userId)
            .get()
            .then(document => document.data() as UserData);
    }

    async set(userId: string, data: UserData): Promise<void> {
        return this.collection.doc(userId).set(data);
    }

    async update(userId: string, data: Partial<UserData>): Promise<void> {
        return this.collection.doc(userId).update(data);
    }

    private get collection() {
        return this.db.collection('userData');
    }
}

export const userDataRepo = new UserDataRepo(db);
