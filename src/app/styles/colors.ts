export const Colors = {
    primary: '#FFDE59',
    background: '#FFF',
    foreground: '#000000',
    facebook: '#2F73E0',
    google: '#CB4023',
    selected: '#a0cc70',
};
