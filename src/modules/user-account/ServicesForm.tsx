import React from 'react';
import { Button } from 'grommet';

import { FancyHeading } from 'app/components';
import { Col, Row } from 'app/components/Grid';
import { Colors } from 'app/styles';
import {
    KeeperServiceKind,
    KeeperServiceOptions,
    KeeperServices,
} from 'modules/user/types';
import { ServiceTile } from './ServiceTile';

type Props = {
    services: KeeperServices;
    setServices(setter: (prv: KeeperServices) => KeeperServices): void;
};

const initialServiceOptions: KeeperServiceOptions = {
    active: false,
    applicablePets: [],
    pricePerDay: 0,
    notes: '',
};

export const ServicesForm: React.FC<Props> = ({ services, setServices }) => {
    const [isKeeper, setKeeper] = React.useState(
        Object.values(services).some(s => s != null)
    );

    function renderTiles(): React.ReactNode {
        return Object.entries(services).map(([kind, data]) => (
            <ServiceTile
                key={kind}
                kind={kind as KeeperServiceKind}
                data={data || initialServiceOptions}
                onChange={data =>
                    setServices(services => ({ ...services, [kind]: data }))
                }
            />
        ));
    }

    if (!isKeeper) {
        return (
            <Row center="xs" padTop="L">
                <Col>
                    <Button
                        onClick={() => setKeeper(true)}
                        color={Colors.primary}
                        label="Zostań opiekunem!"
                    />
                </Col>
            </Row>
        );
    }

    return (
        <>
            <Row center="xs" padTop="L">
                <Col padBot="M">
                    <FancyHeading>Moja oferta</FancyHeading>
                </Col>
            </Row>
            <Row top="xs">{renderTiles()}</Row>
        </>
    );
};
