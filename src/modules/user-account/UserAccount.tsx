import { Col, Row } from 'app/components/Grid';
import { Colors } from 'app/styles/colors';
import EmailValidator from 'email-validator';
import { Button } from 'grommet';
import { UserData } from 'modules/user/types';
import React from 'react';
import { ProfileForm, ServicesForm } from '.';
import Alert from 'app/alerts';

type Props = {
    userData: UserData;
    profilePicUrl?: string;

    onProfilePicChange(image: File): void;
    updateUserData(data: Partial<UserData>): Promise<void>;
    logout(): void;
};

export const UserAccount: React.FC<Props> = props => {
    const [name, setName] = React.useState(props.userData.firstName);
    const [surname, setSurname] = React.useState(props.userData.lastName);
    const [address, setAddress] = React.useState(props.userData.address || '');
    const [email, _] = React.useState(props.userData.email || '');
    const [phone, setPhone] = React.useState(props.userData.phone || '');
    const [about, setAbout] = React.useState(props.userData.about || '');

    const [services, setServices] = React.useState(props.userData.services);

    function submitForm() {
        props
            .updateUserData({
                firstName: name,
                lastName: surname,
                address: address || null,
                phone: phone || null,
                about: about || null,
                services,
            })
            .then(() => Alert.success('Dane zaktualizowane!'));
    }

    const isFormValid = () =>
        name && surname && address && EmailValidator.validate(email);

    return (
        <Row pad="M" end="xs">
            <Col xs>
                <Button
                    onClick={props.logout}
                    color={Colors.foreground}
                    label="Wyloguj"
                />
            </Col>

            <Col xs={12}>
                <ProfileForm
                    name={name}
                    setName={setName}
                    surname={surname}
                    setSurname={setSurname}
                    address={address}
                    setAddress={setAddress}
                    phone={phone}
                    setPhone={setPhone}
                    about={about}
                    setAbout={setAbout}
                    email={email}
                    profilePicUrl={props.profilePicUrl}
                    onProfilePicChange={props.onProfilePicChange}
                />
            </Col>

            <Col xs={12}>
                <ServicesForm services={services} setServices={setServices} />
            </Col>

            <Col xs={12} padY="L">
                <Row center="xs">
                    <Button
                        onClick={submitForm}
                        disabled={!isFormValid()}
                        color={Colors.primary}
                        label="Zapisz zmiany"
                        primary
                    />
                </Row>
            </Col>
        </Row>
    );
};
