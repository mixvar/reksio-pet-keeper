export const Spacing = {
    XS: '4px',
    S: '8px',
    M: '12px',
    L: '24px',
    XL: '48px',
};
