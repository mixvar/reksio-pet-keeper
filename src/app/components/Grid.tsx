import React from 'react';
import styled, { css } from 'styled-components/macro';
import omit from 'lodash/omit';
import {
    Row as FlexRow,
    Col as FlexCol,
    RowProps as FlexRowProps,
    ColProps as FlexColProps,
} from 'react-flexbox-grid';
import { Spacing } from 'app/styles';

type SpacingValue = (keyof (typeof Spacing)) | number;

interface SpacingProps {
    pad?: SpacingValue;
    padX?: SpacingValue;
    padY?: SpacingValue;
    padTop?: SpacingValue;
    padBot?: SpacingValue;
    padLeft?: SpacingValue;
    padRight?: SpacingValue;
}

const toPixels = (spacing: SpacingValue) =>
    (Spacing as any)[spacing] || spacing + 'px';

const spacingMixin = (config: SpacingProps) => css`
    ${config.pad &&
        css`
            padding: ${toPixels(config.pad)};
        `};
    ${config.padX &&
        css`
            padding-left: ${toPixels(config.padX)};
            padding-right: ${toPixels(config.padX)};
        `};
    ${config.padY &&
        css`
            padding-top: ${toPixels(config.padY)};
            padding-bottom: ${toPixels(config.padY)};
        `};
    ${config.padTop &&
        css`
            padding-top: ${toPixels(config.padTop)};
        `};
    ${config.padBot &&
        css`
            padding-bottom: ${toPixels(config.padBot)};
        `};
    ${config.padLeft &&
        css`
            padding-left: ${toPixels(config.padLeft)};
        `};
    ${config.padRight &&
        css`
            padding-right: ${toPixels(config.padRight)};
        `};
`;

const omitExtraProps = (props: any) =>
    omit(props, [
        'pad',
        'padX',
        'padY',
        'padTop',
        'padBot',
        'padLeft',
        'padRight',
        'nowrap',
        'fill',
    ]);

type RowProps = FlexRowProps &
    SpacingProps & { nowrap?: boolean; fill?: boolean };

export const Row = styled((props: RowProps) =>
    React.createElement(FlexRow, omitExtraProps(props))
)<RowProps>`
    && {
        margin: 0;
        ${props => spacingMixin(props)};
        flex-wrap: ${props => (props.nowrap ? 'nowrap' : 'wrap')};
        height: ${props => (props.fill ? '100%' : 'auto')};
    }
`;
type ColProps = FlexColProps & SpacingProps;

export const Col = styled((props: ColProps) =>
    React.createElement(FlexCol, omitExtraProps(props))
)<ColProps>`
    && {
        padding: 0;
        ${props => spacingMixin(props)};
    }
`;
