import React from 'react';
import {
    Accordion,
    AccordionPanel,
    Box,
    Button,
    Text,
    TextArea,
    TextInput,
} from 'grommet';
import { Col, Row } from 'app/components/Grid';
import { Colors } from 'app/styles/colors';
import { Spacing } from 'app/styles';
import { contactRequestRepo } from 'app/repositories';
import EmailValidator from 'email-validator';

type Props = {};

export const About: React.FC<Props> = () => {
    const [email, setEmail] = React.useState('');
    const [text, setText] = React.useState('');
    const [formSend, setFromSend] = React.useState(false);

    function submitForm() {
        contactRequestRepo
            .push({
                mail: email,
                message: text,
            })
            .then(() => setFromSend(true));
    }

    const isFormValid = () => EmailValidator.validate(email) && text;

    return (
        <Row pad="M" fill>
            <Col xs={7} pad="M">
                <h2>Pytania</h2>
                <Accordion>
                    <AccordionPanel label="Czym jest Reskio Pet keeper ?">
                        <Box pad={Spacing.L}>
                            <Text>
                                Reksio jest platformą łączącą właścicieli
                                zwierzaków z idealnymi opiekunami. Chcemy jak
                                najbardiej ułatwić cały proces od wybrania
                                terminu naszego wyjazdu do odebrania naszego
                                pupila od opiekuna po powrocie z wakacji.
                                <br />
                                Naszym celem jest przede wszystkim zapobiegnie
                                porzucaniu zwierząt w okresie wakacyjnym poprzez
                                budowanie świadomości wśród właścicieli zwierząt
                                domowych.
                            </Text>
                        </Box>
                    </AccordionPanel>
                    <AccordionPanel label="Czy w ramach portalu jesteśmy przeprowadzić cały proces realizacji usługi?">
                        <Box pad={Spacing.L}>
                            <Text>
                                Tak w ramach portalu zapewniamy bezpieczeństwo
                                bezgotówkowej transakcji między właścicielem
                                zwierzaka a opiekunem. Za pośrednictwo odpowiada
                                (tutaj nazwa tego ze czego będziemy korzystać)
                            </Text>
                        </Box>
                    </AccordionPanel>
                    <AccordionPanel label="Jakie usługi można zarezerwować w ramach naszego portalu?">
                        <Box pad={Spacing.L}>
                            <Text>
                                Główna usługą jaką oferują nasi opiekunowie jest
                                opieka nad zwierzęciem w domu opiekuna lub
                                właściciela oraz pomoc opiekuna przy
                                wyprowadzaniu na spacer naszego pupila{' '}
                            </Text>
                        </Box>
                    </AccordionPanel>
                    <AccordionPanel label="Jak wybrać właściwego opiekuna?">
                        <Box pad={Spacing.L}>
                            <Text>
                                Wiemy że wybór własciewego opiekuna dla naszego
                                pupila jest nie lada wyzwaniem. W końcu nie
                                zostawimy naszego człąka rodziny byle komu.
                                Dlatego Reksio zapewnia rozbudowany sytem opinii
                                opiekunów ułatwiający znalezienie tego jedynego.
                                Cenimy też wartość polecenia, dlatego w prosty
                                sposób możecie polecić swojego ulubionego
                                opiekuna innym użytkowniką. Ponad to zachęcamy
                                do wspólnego spaceru z opekunem przed
                                zostawieniem u niego naszego ukochanego
                                zwierzątka.{' '}
                            </Text>
                        </Box>
                    </AccordionPanel>
                    <AccordionPanel label="Kto może zostać opiekunem?">
                        <Box pad={Spacing.L}>
                            <Text>
                                Opiekunem może zostać każda osoba która
                                ukończyła 16 lat oraz bierze odpowiedzialność za
                                zwierzaka w czasie opieki{' '}
                            </Text>
                        </Box>
                    </AccordionPanel>
                </Accordion>
            </Col>

            <Col xs={5} pad="M">
                <Row>
                    <h2>
                        Nie znalazłeś odpowiedzi na swoje pytanie? <br /> Zadaj
                        je nam.
                    </h2>
                </Row>
                <Row padY="S">
                    <TextInput
                        placeholder="Twój e-mail"
                        value={email}
                        type="email"
                        onChange={event => setEmail(event.target.value)}
                    />
                </Row>
                <Row padY="S">
                    <TextArea
                        placeholder="W czym możemy Ci pomóc?"
                        rows={6}
                        value={text}
                        onChange={(event: any) => setText(event.target.value)}
                    />
                </Row>
                <Row padY="S" center="xs">
                    <Button
                        onClick={submitForm}
                        color={Colors.primary}
                        disabled={!isFormValid()}
                        label="Wyślij"
                        primary
                    />
                </Row>

                {formSend && <Text>Wiadomość wysłana pomyślnie</Text>}
            </Col>
        </Row>
    );
};
