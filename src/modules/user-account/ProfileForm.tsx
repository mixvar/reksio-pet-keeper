import { FancyHeading } from 'app/components';
import { Col, Row } from 'app/components/Grid';
import { Box, Button, Image, Stack, TextArea, TextInput } from 'grommet';
import React from 'react';
import { ImagePicker } from './ImagePicker';

type Props = {
    name: string;
    setName(v: string): void;

    surname: string;
    setSurname(v: string): void;

    address: string;
    setAddress(v: string): void;

    phone: string;
    setPhone(v: string): void;

    about: string;
    setAbout(v: string): void;

    email: string;

    profilePicUrl?: string;
    onProfilePicChange(image: File): void;
};

export const ProfileForm: React.FC<Props> = props => {
    const [imagePickerOpen, setImagePickerOpen] = React.useState(false);

    return (
        <>
            <Row>
                <Col xs={12}>
                    <Row padBot="M" center="xs">
                        <FancyHeading>Mój profil</FancyHeading>
                    </Row>
                </Col>

                <Col xs={12} md={6} padX="M">
                    {props.profilePicUrl && (
                        <Row padY="S">
                            <Stack anchor="bottom-left">
                                <Box height="small" width="small" round="large">
                                    <Image
                                        style={{ borderRadius: '70%' }}
                                        fit="cover"
                                        src={props.profilePicUrl}
                                    />
                                </Box>
                                <Button
                                    plain
                                    label="Aktualizuj"
                                    onClick={() => setImagePickerOpen(true)}
                                />
                            </Stack>
                        </Row>
                    )}
                    <Row padY="S">
                        <TextInput
                            placeholder="Imię"
                            value={props.name}
                            onChange={event =>
                                props.setName(event.target.value)
                            }
                        />
                    </Row>
                    <Row padY="S">
                        <TextInput
                            placeholder="Nazwisko"
                            value={props.surname}
                            onChange={event =>
                                props.setSurname(event.target.value)
                            }
                        />
                    </Row>
                </Col>

                <Col xs={12} md={6} padX="M">
                    <Row padY="S">
                        <TextInput
                            placeholder="Email"
                            type="email"
                            value={props.email}
                            disabled
                        />
                    </Row>
                    <Row padY="S">
                        <TextInput
                            placeholder="Numer telefonu"
                            value={props.phone}
                            onChange={event =>
                                props.setPhone(event.target.value)
                            }
                        />
                    </Row>
                    <Row padY="S">
                        <TextInput
                            placeholder="Adres"
                            value={props.address}
                            onChange={event =>
                                props.setAddress(event.target.value)
                            }
                        />
                    </Row>
                    <Row padY="S">
                        <TextArea
                            rows={6}
                            placeholder="Kilka słów o mnie"
                            value={props.about}
                            onChange={(event: any) =>
                                props.setAbout(event.target.value)
                            }
                        />
                    </Row>
                </Col>
            </Row>

            <ImagePicker
                open={imagePickerOpen}
                onClose={() => setImagePickerOpen(false)}
                onImageSelected={image => {
                    setImagePickerOpen(false);
                    props.onProfilePicChange(image);
                }}
            />
        </>
    );
};
