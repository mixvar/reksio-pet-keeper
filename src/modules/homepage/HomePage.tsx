import React from 'react';
import styled from 'styled-components/macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from 'grommet';
import { Col, Row } from 'app/components/Grid';
import { Colors } from 'app/styles/colors';
import { MapVisibility, useMap } from 'modules/map';

const StyledMessage = styled.h1`
    font-size: 60px;
    font-weight: 500;
    font-weight: bold;
`;
const StyledMarkerIcon = styled(FontAwesomeIcon)`
    font-size: 200px;
    font-weight: 500;
    color: ${Colors.primary};
    margin-right: 20px;
`;

type Props = {};

export const HomePage: React.FC<Props> = () => {
    const { setMapVisibility, setLocation } = useMap();

    const getLocation = () => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                console.log(position);
                setLocation(position);
            });
        }
    };

    React.useEffect(() => {
        getLocation();
    }, []);

    return (
        <div>
            <Row pad="L">
                <Col xs={7} lg={12}>
                    <Row padY="XL" middle="xs" nowrap>
                        <StyledMarkerIcon icon="map-marker-alt" />
                        <StyledMessage>
                            Znajdź opiekuna <br /> dla swojego pupila
                        </StyledMessage>
                    </Row>
                </Col>

                <Col xs={12}>
                    <Row end="xs">
                        <Col pad="S">
                            <Button
                                onClick={() =>
                                    setMapVisibility(MapVisibility.Large)
                                }
                                color={Colors.primary}
                                label="expand map"
                            />
                        </Col>
                        <Col pad="S">
                            <Button
                                onClick={() =>
                                    setMapVisibility(MapVisibility.Small)
                                }
                                color={Colors.primary}
                                label="collapse map"
                            />
                        </Col>
                        <Col pad="S">
                            <Button
                                onClick={() =>
                                    setMapVisibility(MapVisibility.Hidden)
                                }
                                color={Colors.primary}
                                label="hide map"
                            />
                        </Col>
                        <Col pad="S">
                            <Button
                                onClick={() => getLocation()}
                                color={Colors.primary}
                                label="getLocation"
                            />
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    );
};
