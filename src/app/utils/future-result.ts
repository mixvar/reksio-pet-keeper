/**
 * Algebraic Data Type representing data that can be loaded asynchronously and its status
 * @see http://blog.jenkster.com/2016/06/how-elm-slays-a-ui-antipattern.html
 */
export type Type<P, E = Error> = FutureResult<P, E>;

type FutureResult<P, E> =
    | Initial
    | Loading
    | Success<P>
    | Updating<P>
    | Failure<E>;

interface Initial {
    readonly status: 'initial';
}
interface Loading {
    readonly status: 'loading';
}
interface Success<P> {
    readonly status: 'success';
    readonly payload: P;
}
interface Updating<P> {
    readonly status: 'updating';
    readonly payload: P;
}
interface Failure<E> {
    readonly status: 'failure';
    readonly error: E;
}

/* creators */
export function initial(): Initial {
    return { status: 'initial' };
}
export function loading(): Loading {
    return { status: 'loading' };
}
export function success<P>(payload: P): Success<P> {
    return { payload, status: 'success' };
}
export function updating<P>(payload: P): Updating<P> {
    return { payload, status: 'updating' };
}
export function failure<E>(error: E): Failure<E> {
    return { error, status: 'failure' };
}

/* typeguards */
export function isInitial(it: FutureResult<unknown, unknown>): it is Initial {
    return it.status === 'initial';
}
export function isLoading(it: FutureResult<unknown, unknown>): it is Loading {
    return it.status === 'loading';
}
export function isSuccess<P>(it: FutureResult<P, unknown>): it is Success<P> {
    return it.status === 'success';
}
export function isUpdating<P>(it: FutureResult<P, unknown>): it is Updating<P> {
    return it.status === 'updating';
}
export function isFailure<E>(it: FutureResult<unknown, E>): it is Failure<E> {
    return it.status === 'failure';
}

/* utils */
export function get<P>(it: FutureResult<P, unknown>): P | null {
    return isSuccess(it) || isUpdating(it) ? it.payload : null;
}

export function reduce<P, E, O>(
    it: FutureResult<P, E>,
    handlers: {
        initial: () => O;
        loading: () => O;
        success: (payload: P) => O;
        updating: (payload: P) => O;
        failure: (error: E) => O;
    }
): O {
    switch (it.status) {
        case 'initial':
            return handlers.initial();
        case 'loading':
            return handlers.loading();
        case 'success':
            return handlers.success(it.payload);
        case 'updating':
            return handlers.updating(it.payload);
        case 'failure':
            return handlers.failure(it.error);
    }
}
