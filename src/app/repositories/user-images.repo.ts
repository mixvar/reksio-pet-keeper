import firebase, { storage } from 'firebase';

import DEFAULT_PROFILE_PIC from 'app/assets/reksio-logo.png';

class UserImagesRepo {
    constructor(private storage: storage.Storage) {}

    async getProfilePictureUrl(userId: string): Promise<string> {
        try {
            return await this.profilePicturesRef
                .child(userId + '.jpg')
                .getDownloadURL();
        } catch {
            return DEFAULT_PROFILE_PIC;
        }
    }

    async uploadProfilePicture(userId: string, image: File): Promise<string> {
        const metadata: storage.UploadMetadata = {
            contentType: image.type,
        };

        return this.profilePicturesRef
            .child(userId + '.jpg')
            .put(image, metadata)
            .then(snapshot => snapshot.ref.getDownloadURL());
    }

    private get profilePicturesRef() {
        return this.storage.ref().child('profile_pictures');
    }
}

export const userImagesRepo = new UserImagesRepo(firebase.storage());
