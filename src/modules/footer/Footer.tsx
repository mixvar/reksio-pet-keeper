import React from 'react';
import { Button } from 'grommet';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import useReactRouter from 'use-react-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons/faFacebookSquare';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { Colors } from 'app/styles';
import { Col, Row } from 'app/components/Grid';
import logo from 'app/assets/logo_kp.png';

const StyledFooter = styled(Row)<{ height: number }>`
    min-height: ${({ height }) => `${height}px`};
    algin-items: end;
`;

const StyledMarkerIcon = styled(FontAwesomeIcon)`
    font-size: 30px;
    font-weight: 500;
    color: ${Colors.foreground};
    padding: 7px;
    margin-top: -10px;
`;

const StyledLogo = styled.img`
    width: 75px;
    padding: 7px;
    margin-top: -15px;
`;

const StyledText = styled.div`
    font-size: 12px;
    line-height: 21px;
`;

type Props = {
    height: number;
};

export const Footer: React.FC<Props> = ({ height }) => {
    const router = useReactRouter();
    const path = router.location.pathname;

    return (
        <StyledFooter around="xs" height={height}>
            <Col>
                <StyledText>© REKSIO 2019</StyledText>
            </Col>
            <Col>
                <Link to="/regulations">
                    <Button
                        label="REGULAMIN"
                        active={path === '/regulations'}
                        plain
                        hoverIndicator
                        color={Colors.foreground}
                        style={{ fontSize: 12 }}
                    />
                </Link>
            </Col>
            <Col>
                <Link to="/privacy-policy">
                    <Button
                        label="POLITYKA PRYWATNOŚCI"
                        active={path === '/privacy-policy'}
                        plain
                        hoverIndicator
                        color={Colors.foreground}
                        style={{ fontSize: 12 }}
                    />
                </Link>
            </Col>
            <Col>
                <Row>
                    <StyledText>ZNAJDŹ NAS</StyledText>

                    <a
                        target="_blank"
                        href="https://www.facebook.com/Reksio-petkeeper-573885869780862/"
                    >
                        <StyledMarkerIcon icon={faFacebookSquare} />
                    </a>
                    <a
                        target="_blank"
                        href="https://www.instagram.com/reksio_petkeeper/"
                    >
                        <StyledMarkerIcon icon={faInstagram} />
                    </a>
                </Row>
            </Col>
            <Col>
                <Row>
                    <StyledText>PARTNERZY</StyledText>
                    <a target="_blank" href="https://www.karmimypsiaki.pl/">
                        <StyledLogo src={logo} alt="Karmimy Psiaki Logo" />
                    </a>
                </Row>
            </Col>
        </StyledFooter>
    );
};
