import React from 'react';

type Props = {};

export const KeeperListing: React.FC<Props> = () => (
    <h2>Opiekunowie w Twojej okolicy</h2>
);
