import React from 'react';

export enum MapVisibility {
    Hidden,
    Small,
    Large,
}

export type MapContextType = {
    mapVisibility: MapVisibility;
    setMapVisibility(v: MapVisibility): void;
    location: Position;
    setLocation(v: Position): void;
};

const ReactContext = React.createContext<MapContextType | null>(null);

export const MapContextProvider: React.FC = ({ children }) => {
    const [mapVisibility, setMapVisibility] = React.useState(
        MapVisibility.Small
    );
    const [location, setLocation] = React.useState();

    const value = React.useMemo(
        () => ({ mapVisibility, setMapVisibility, location, setLocation }),
        [mapVisibility, location]
    );

    return <ReactContext.Provider value={value} children={children} />;
};

export function useMap(): MapContextType {
    const context = React.useContext(ReactContext);
    if (context == null) {
        throw new Error(`useMap must be used within a MapContextProvider`);
    }
    return context;
}
