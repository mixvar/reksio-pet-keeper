import React from 'react';
import { Map as LeafletMapInstance } from 'leaflet';
import { useLeaflet } from 'react-leaflet';
import styled, { css } from 'styled-components/macro';

const StyledMapOverlay = styled.div<{ fillSpace?: boolean }>`
    position: absolute;
    z-index: 1000;
    left: 0;
    top: 0;
    ${({ fillSpace }) =>
        fillSpace &&
        css`
            width: 100%;
            height: 100%;
        `}
`;

type Props = {
    className?: string;
    fill?: boolean;
    blocking?: boolean;
};

export const MapOverlay: React.FC<Props> = ({
    children,
    className,
    fill,
    blocking,
}) => {
    const { map } = useLeaflet();

    React.useEffect(() => {
        if (blocking && map) disableInteractions(map);

        return () => {
            map && enableInteractions(map);
        };
    }, [blocking]);

    return (
        <StyledMapOverlay fillSpace={fill} className={className}>
            {children}
        </StyledMapOverlay>
    );
};

function enableInteractions(map: LeafletMapInstance): void {
    map.dragging.enable();
    map.touchZoom.enable();
    map.doubleClickZoom.enable();
    map.scrollWheelZoom.enable();
    map.boxZoom.enable();
    map.keyboard.enable();
    map.tap && map.tap.enable();
}

function disableInteractions(map: LeafletMapInstance): void {
    map.dragging.disable();
    map.touchZoom.disable();
    map.doubleClickZoom.disable();
    map.scrollWheelZoom.disable();
    map.boxZoom.disable();
    map.keyboard.disable();
    map.tap && map.tap.disable();
}
